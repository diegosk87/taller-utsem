package com.example.text;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText name;
    private Button mas;
    private Button menos;
    private TextView text;
    int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mas = findViewById(R.id.mas);
        menos = findViewById(R.id.menos);
        text = findViewById(R.id.text);

        mas.setOnClickListener(this);
        menos.setOnClickListener(this);
        text.setText(contador +  "");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mas:
                if(contador < 20)
                    contador++;
                break;
            case R.id.menos:
                if(contador > 0)
                    contador--;
        }
        text.setText(contador +  "");
    }
}
